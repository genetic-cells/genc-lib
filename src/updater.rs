use crate::entity::Entity;
use crate::pos::{Pos, PosOffset};
use crate::processor::CommandProcessor;
use crate::world::{Layer, OptionLayer, WCell, World};
use rand::prelude::SmallRng;

pub struct Updater {
    proc: CommandProcessor,
}

impl Updater {
    pub fn new() -> Self {
        Self {
            proc: CommandProcessor::default(),
        }
    }

    pub fn update(&mut self, world: &mut World, rng: &mut SmallRng) {
        let some_count = world.entities().some_count();
        let mut to_update = Vec::with_capacity(some_count);
        world.entities_mut().get_somes(&mut to_update);

        for mut p in to_update {
            let mut ctx = UpdateCtx::new(world, &mut p);
            self.proc.process_entity(&mut ctx, rng);
        }
    }
}

///
/// Обертка для соблюдения контрактов Updater's
///
/// А именно, запрещает (или делает валидным), перемещения сущностей помимо текущей
pub struct UpdateCtx<'a> {
    world: &'a mut World,
    current: &'a mut Pos,
}

impl<'a> UpdateCtx<'a> {
    pub fn new(world: &'a mut World, current: &'a mut Pos) -> Self {
        Self { world, current }
    }

    pub fn size(&self) -> [usize; 2] {
        self.world.size()
    }

    pub fn width(&self) -> usize {
        self.world.width()
    }
    pub fn height(&self) -> usize {
        self.world.height()
    }

    pub fn world(&self) -> &World {
        &self.world
    }
    pub unsafe fn world_mut(&mut self) -> &mut World {
        &mut self.world
    }

    pub fn entities(&self) -> &OptionLayer<Entity> {
        self.world.entities()
    }
    pub unsafe fn entities_mut(&mut self) -> &mut OptionLayer<Entity> {
        self.world_mut().entities_mut()
    }

    pub fn add_entity(&mut self, pos: Pos, entity: Entity) -> Result<(), Entity> {
        self.world.entities_mut().add(pos, entity)
    }
    pub fn add_entity_by(
        &mut self,
        offset: impl Into<PosOffset>,
        entity: Entity,
    ) -> Result<(), Entity> {
        self.world
            .entities_mut()
            .add(*self.current + offset.into(), entity)
    }

    pub fn terrain(&self) -> &Layer<WCell> {
        self.world.terrain()
    }
    pub fn terrain_mut(&mut self) -> &mut Layer<WCell> {
        self.world.terrain_mut()
    }

    pub fn cell(&self, pos: Pos) -> &WCell {
        self.world.cell(pos)
    }
    pub fn cell_mut(&mut self, pos: Pos) -> &mut WCell {
        self.world.cell_mut(pos)
    }

    pub fn current_cell(&self) -> &WCell {
        self.cell(*self.current)
    }
    pub fn current_cell_mut(&mut self) -> &mut WCell {
        self.cell_mut(*self.current)
    }

    pub fn population(&self) -> usize {
        self.world.population()
    }

    pub fn move_to(&mut self, to: Pos) -> Result<(), ()> {
        let ent = self.world.entities_mut();
        if ent.is_none(to) {
            let e = ent.take(*self.current).unwrap();
            ent.replace(to, e);
            *self.current = to;
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn move_by(&mut self, offset: impl Into<PosOffset>) -> Result<(), ()> {
        self.move_to(*self.current + offset.into())
    }

    pub fn current_pos(&self) -> Pos {
        *self.current
    }

    pub fn current(&self) -> &Entity {
        self.world.entities().get(*self.current).unwrap()
    }

    pub fn current_mut(&mut self) -> &mut Entity {
        self.world.entities_mut().get_mut(*self.current).unwrap()
    }

    pub fn look_at(&self) -> Pos {
        self.current_pos() + self.current().look_at()
    }
}
