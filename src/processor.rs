extern crate num_enum;

use rand::prelude::SmallRng;
use rand::Rng;

use crate::processor::Action::{Break, Continue};
use crate::updater::UpdateCtx;
use crate::world::World;

// Fn(UpdateCtx<'a>, &EntityProcSettings, &mut SmallRng) -> Action;

// --------
// Settings
// --------

pub struct EntityProcSettings {
    pub logic_cmd_limit: usize,
    pub en_on_idle: i32,
    pub en_on_move: i32,
    pub light_per_en: i32,
}

impl Default for EntityProcSettings {
    fn default() -> Self {
        Self {
            logic_cmd_limit: 256,
            en_on_idle: -1,
            en_on_move: -2,
            light_per_en: 64,
        }
    }
}

pub enum Action {
    Continue,
    Break,
}

// ---------
// Processor
// ---------

pub struct CommandProcessor {
    cmds: Box<[Option<Box<dyn Fn(&mut UpdateCtx, &EntityProcSettings, &mut SmallRng) -> Action>>]>,
    settings: EntityProcSettings,
    post_handlers: Vec<fn(&mut World, &EntityProcSettings, &mut SmallRng)>,
}

impl CommandProcessor {
    pub fn new() -> Self {
        let cmds = (0..256)
            .map(|_| None)
            .collect::<Vec<_>>()
            .into_boxed_slice();
        Self {
            cmds,
            settings: EntityProcSettings::default(),
            post_handlers: Vec::new(),
        }
    }

    pub fn add_post_handler(&mut self, f: fn(&mut World, &EntityProcSettings, &mut SmallRng)) {
        self.post_handlers.push(f);
    }

    pub fn set_cmd(
        &mut self,
        index: u8,
        handler: impl Fn(&mut UpdateCtx, &EntityProcSettings, &mut SmallRng) -> Action + 'static,
    ) {
        self.cmds[index as usize].replace(Box::new(handler));
    }

    pub fn process_entity(&self, ctx: &mut UpdateCtx, rng: &mut SmallRng) {
        let cmd_limit = self.settings.logic_cmd_limit;

        let mut logic_cmd = 0;

        while logic_cmd < cmd_limit {
            let opcode = ctx.current_mut().dna.read_u8() as usize;
            let handler = (&self.cmds[opcode].as_ref()).map(|b| b.as_ref());

            match handler {
                Some(handler) => {
                    let action = handler(ctx, &self.settings, rng);
                    match action {
                        Action::Continue => logic_cmd += 1,
                        Action::Break => break,
                    }
                }
                None => logic_cmd += 1,
            }
        }
    }
}

const MUT_FACTOR: f64 = 0.15;

impl Default for CommandProcessor {
    fn default() -> Self {
        let mut proc = CommandProcessor::new();

        use crate::codes::*;
        proc.set_cmd(NOP, |c, s, _| {
            c.current_mut().add_en(s.en_on_idle);
            Break
        });
        proc.set_cmd(MOVE, |c, s, _r| {
            let to = c.current().look_at();
            match c.move_by(to) {
                Ok(_) => c.current_mut().add_en(s.en_on_move),
                Err(_) => c.current_mut().add_en(s.en_on_idle),
            }
            Break
        });
        proc.set_cmd(ROTATE, |c, _, _| {
            let a = c.current_mut().dna.read_u8();
            c.current_mut().rotate(a);
            Continue
        });
        proc.set_cmd(ROTATE_RIGHT, |c, _, _| {
            c.current_mut().rotate(1);
            Continue
        });
        proc.set_cmd(ROTATE_LEFT, |c, _, _| {
            c.current_mut().rotate(7);
            Continue
        });
        proc.set_cmd(ROTATE_OPS, |c, _, _| {
            let a = c.current_mut().ops.pop();
            c.current_mut().rotate(a);
            Continue
        });
        proc.set_cmd(DIVIDE, |c, s, r| {
            let to = c.look_at();
            if c.entities().is_none(to) {
                let child = c.current_mut().divide(MUT_FACTOR, r);
                let _x = child
                    .map(|child| c.add_entity(to, child))
                    .map_err(|_| c.current_mut().add_en(s.en_on_idle));
            } else {
                c.current_mut().add_en(s.en_on_idle);
            }
            Break
        });
        proc.set_cmd(PHOTOSYNTHESIZE, |c, s, _r| {
            let en = c.current_cell().light as i32 / s.light_per_en;
            let en = en;
            c.current_mut().add_en(en);
            Break
        });
        proc.set_cmd(IF, |c, _s, _r| {
            let e = c.current_mut();
            let ptr_offset = e.dna.read_i8() as isize;
            e.dna.skipi(ptr_offset);
            Continue
        });
        proc.set_cmd(LT_ARG, |c, _s, _r| {
            let e = c.current_mut();
            let value = e.ops.pop();
            let arg = e.dna.read_u8();
            e.ops.push_bool(value < arg);
            Continue
        });

        proc.set_cmd(PUSH_RAND, |c, _s, r| {
            c.current_mut().ops.push(r.gen());
            Continue
        });

        proc.set_cmd(PUSH_SELF_EN, |c, _s, _r| {
            let en = c.current().en();
            c.current_mut().ops.push(en);
            Continue
        });

        proc
    }
}
