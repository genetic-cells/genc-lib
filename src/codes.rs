




/// Postfixes:
/// - _OPS -- аргумент взят из стека операндов
///


// 0x0*
// base
pub const NOP: u8 = 0x00;
// TODO: Add 'IDLE' cmd
pub const MOVE: u8 = 0x01;

pub const ROTATE: u8 = 0x02;
pub const ROTATE_RIGHT: u8 = 0x03;
pub const ROTATE_LEFT: u8 = 0x04;
pub const ROTATE_OPS: u8 = 0x05;

pub const DIVIDE: u8 = 0x06;
pub const DIVIDE_PTR: u8 = 0x07;
pub const DIVIDE_GOTO: u8 = 0x08;
//pub const DIVIDE_GOTO_ZERO: u8 = 0x09;

pub const ATTACK: u8 = 0x0A;
pub const _: u8 = 0x0B;

pub const PHOTOSYNTHESIZE: u8 = 0x0C;
//pub const CHEMOSYNTHESIZE: u8 = 0x0D;

pub const _: u8 = 0x0E;
pub const _: u8 = 0x0F;

// 0x1*
// logical
pub const GOTO: u8 = 0x10;
pub const GOTO_ABSOLUTE: u8 = 0x11;

pub const IF: u8 = 0x12;
pub const IF_EQ: u8 = 0x13;

pub const GT: u8 = 0x14;
pub const LT: u8 = 0x15;
pub const GT_ARG: u8 = 0x16;
pub const LT_ARG: u8 = 0x17;

pub const EQ: u8 = 0x18;
pub const EQ_ARG: u8 = 0x19;

pub const IF_EQ_HASH: u8 = 0x1A;
pub const _: u8 = 0x1B;

pub const NOT: u8 = 0x1C;
pub const AND: u8 = 0x1D;
pub const OR: u8 = 0x1E;
pub const XOR: u8 = 0x1F;

// 0x2*
// op_stack
pub const PUSH_0: u8 = 0x20;
pub const PUSH_1: u8 = 0x21;
pub const PUSH_4: u8 = 0x22;
pub const PUSH_16: u8 = 0x23;
pub const PUSH_64: u8 = 0x24;
pub const PUSH_255: u8 = 0x25;
pub const PUSH: u8 = 0x26;
pub const PUSH2: u8 = 0x27;

pub const POP: u8 = 0x28;
pub const POP2: u8 = 0x29;

pub const SWAP: u8 = 0x2A;
pub const _: u8 = 0x2B;

pub const DUP: u8 = 0x2C;
pub const DUP2: u8 = 0x2D;

pub const _: u8 = 0x2E;
pub const PUSH_RAND: u8 = 0x2F;

// 0x3*
// sensors
pub const PUSH_SELF_HASH: u8 = 0x30;
pub const PUSH_SELF_EN: u8 = 0x31;
pub const PUSH_SELF_HP: u8 = 0x32;
pub const _: u8 = 0x33;
pub const PUSH_IS_CAN_MOVE: u8 = 0x34;
pub const PUSH_IS_ENTITY: u8 = 0x35;
pub const PUSH_LIGHT_ON: u8 = 0x36;
pub const PUSH_ENTITY_HASH: u8 = 0x37;
pub const _: u8 = 0x38;
pub const _: u8 = 0x39;
pub const _: u8 = 0x3A;
pub const _: u8 = 0x3B;
pub const _: u8 = 0x3C;
pub const _: u8 = 0x3D;
pub const _: u8 = 0x3E;
pub const _: u8 = 0x3F;


// 0x4*
// math
pub const ADD: u8 = 0x40;
pub const ADD_ARG: u8 = 0x41;
pub const ADD_SATURATING: u8 = 0x42;
pub const ADD_SATURATING_ARG: u8 = 0x43;
pub const _: u8 = 0x44;
pub const _: u8 = 0x45;
pub const _: u8 = 0x46;
pub const _: u8 = 0x47;
pub const _: u8 = 0x48;
pub const _: u8 = 0x49;
pub const _: u8 = 0x4A;
pub const _: u8 = 0x4B;
pub const _: u8 = 0x4C;
pub const _: u8 = 0x4D;
pub const _: u8 = 0x4E;
pub const _: u8 = 0x4F;