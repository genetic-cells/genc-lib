use num_traits::Signed;
use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Range};

//#[derive(Copy, Clone, Hash, Eq, PartialEq, Default, Debug)]
//pub struct Pos<T: Signed = isize> {
//    pub x: T,
//    pub y: T
//}

// ---
// Pos
// ---

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct Pos {
    pub x: isize,
    pub y: isize,
}

impl Pos {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    pub fn loop_on(&self, [rx, ry]: [Range<isize>; 2]) -> Self {
        Self {
            x: looped(self.x, rx),
            y: looped(self.y, ry),
        }
    }
}

impl Add<[isize; 2]> for Pos {
    type Output = Self;

    fn add(self, rhs: [isize; 2]) -> Self::Output {
        Self::new(self.x + rhs[0], self.y + rhs[1])
    }
}

impl Add<PosOffset> for Pos {
    type Output = Self;

    fn add(self, rhs: PosOffset) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl AddAssign<[isize; 2]> for Pos {
    fn add_assign(&mut self, rhs: [isize; 2]) {
        *self = self.clone() + rhs;
    }
}

impl From<[isize; 2]> for Pos {
    fn from(arr: [isize; 2]) -> Self {
        Pos::new(arr[0], arr[1])
    }
}

impl Display for Pos {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[inline]
pub fn looped<T: Signed + Copy>(val: T, Range { start, end }: Range<T>) -> T {
    let range_len = end - start;
    let r = ((val - start + range_len) % range_len) + start;
    r
}

// ---------
// PosOffset
// ---------

pub struct PosOffset {
    pub x: isize,
    pub y: isize,
}

impl PosOffset {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }
}

impl From<[isize; 2]> for PosOffset {
    fn from([x, y]: [isize; 2]) -> Self {
        Self { x, y }
    }
}

// -------------
// ### Tests ###
// -------------

#[cfg(test)]
mod tests {

    #[test]
    fn looped_works() {
        use super::looped;

        assert_eq!(looped(4, 2..5), 4);
        assert_eq!(looped(5, 2..5), 2);
        assert_eq!(looped(12, 2..5), 3);
        assert_eq!(looped(-1, 2..5), 2);
        assert_eq!(looped(-6, 2..5), 3);

        assert_eq!(looped(4, -4..-1), -2);
        assert_eq!(looped(5, -4..-1), -4);
        assert_eq!(looped(12, -4..-1), -3);
        assert_eq!(looped(-1, -4..-1), -4);
        assert_eq!(looped(-6, -4..-1), -3);
    }
}
