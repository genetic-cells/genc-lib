use rand::Rng;
use std::cmp::min;
use std::ops::Add;

use crate::dna::*;
use crate::pos::PosOffset;
use rand::prelude::SmallRng;

const OPS_SIZE: usize = 8;
const VARS_COUNT: usize = 4;
const LIFETIME_LIMIT: u32 = 4_000_000_000;

const ROT_COUNT: u8 = 8;
/// Cell rotation in 8 directions.
/// Use 3 bits of number
#[derive(Ord, PartialOrd, Eq, PartialEq, Hash, Copy, Clone, Default, Debug)]
pub struct Rotation(u8);

impl Rotation {
    #[inline(always)]
    pub const fn new(angle: u8) -> Self {
        Self(angle % ROT_COUNT)
    }

    pub fn look_at(&self) -> PosOffset {
        match self.0 % 8 {
            0 => [0, -1].into(),
            1 => [1, -1].into(),
            2 => [1, 0].into(),
            3 => [1, 1].into(),
            4 => [0, 1].into(),
            5 => [-1, 1].into(),
            6 => [-1, 0].into(),
            7 => [-1, -1].into(),
            _ => unreachable!(),
        }
    }
}

impl<T> Add<T> for Rotation
where
    T: Into<Self>,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        Self::new(self.0 + rhs.into().0)
    }
}

// ----------
// Entity
// ----------

#[derive(Clone)]
pub struct Entity {
    pub dna: Dna256,
    pub sys_bits: u8,
    pub mark: u8,
    pub ops: OpStack<u8>,
    pub vars: [u8; VARS_COUNT],
    rotation: u8,
    pub lifetime: u32,
    pub en: u8,
}

impl Entity {
    pub fn new(dna: Dna256) -> Self {
        Self {
            dna,
            sys_bits: 0,
            mark: 0,
            ops: OpStack::new(OPS_SIZE),
            vars: [u8::default(); VARS_COUNT],
            rotation: 0,
            lifetime: 0,
            en: 50u8,
        }
    }

    pub fn from_rng<R: Rng + ?Sized>(rng: &mut R) -> Self {
        let mut e = Entity::new(Dna256::from_rng(rng));
        e.rotation = rng.gen_range(0, 8);
        e.en = rng.gen_range(30, 100).into();
        e.mark = rng.gen();
        e
    }

    pub fn inc_lifetime(&mut self) {
        self.lifetime = min(self.lifetime + 1, LIFETIME_LIMIT)
    }

    pub fn look_at(&self) -> crate::pos::PosOffset {
        let rotation = self.rotation;
        Rotation::new(rotation).look_at()
    }

    pub fn rotate(&mut self, angle: u8) {
        self.rotation = (self.rotation + angle % 8) % 8;
    }

    pub fn rotation(&self) -> u8 {
        self.rotation
    }

    pub fn set_rotation(&mut self, rotation: u8) {
        self.rotation = rotation % 8
    }

    pub fn en(&self) -> u8 {
        self.en
    }

    pub fn set_en(&mut self, en: u8) {
        self.en = en
    }

    pub fn add_en(&mut self, en: impl Into<i32>) {
        let en = en.into();
        let self_en = self.en as i32;
        let en = self_en + en;

        if en < 0 {
            self.en = 0
        } else if en > 255 {
            self.en = 255
        } else {
            self.en = en as u8
        }
    }

    // pub fn add_hp(&mut self, hp: impl Into<i32>) {
    //     let hp = hp.into();
    //     let self_hp = self.hp as i32;
    //     let hp = self_hp + hp;
    //
    //     if hp < 0 { self.hp = 0 }
    //     else if hp > 255 { self.hp = 255 }
    //     else { self.hp = hp as u8 }
    // }

    pub fn divide(&mut self, mut_factor: f64, rng: &mut SmallRng) -> Result<Self, ()> {
        if self.en < 2 {
            return Err(());
        }

        let Self {
            dna,
            sys_bits,
            mark,
            rotation,
            en,
            ..
        } = self;

        let mut child_dna = dna.clone();
        child_dna.mutate_with_chance(rng, mut_factor);

        let child_en = *en / 2;
        *en -= child_en;

        Ok(Self {
            dna: child_dna,
            sys_bits: *sys_bits,
            mark: *mark,
            ops: OpStack::new(8),
            vars: [0u8; 4],
            rotation: *rotation,
            lifetime: 0,
            en: child_en,
        })
    }
}

#[derive(Clone)]
pub struct OpStack<T = u8>
where
    T: Default,
{
    data: Vec<T>,
}

impl<T> OpStack<T>
where
    T: Default,
{
    pub fn new(cap: usize) -> Self {
        Self {
            data: Vec::<T>::with_capacity(cap),
        }
    }
}

impl<T> OpStack<T>
where
    T: Default,
{
    pub fn push(&mut self, value: T) {
        if self.data.len() < self.data.capacity() {
            self.data.push(value);
        }
    }

    pub fn pop(&mut self) -> T {
        self.data.pop().unwrap_or(T::default())
    }
}

impl OpStack<u8> {
    pub fn pop_bool(&mut self) -> bool {
        self.pop() != 0
    }

    pub fn push_bool(&mut self, value: bool) {
        self.push(if value { 1 } else { 0 });
    }
}
