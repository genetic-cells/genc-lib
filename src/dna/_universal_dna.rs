use crate::dna::Gene;
use num_traits::{One, Zero};
use rand::prelude::*;
use std::ops::{AddAssign, Index, RemAssign};
use std::rc::Rc;

pub trait DnaData<T = u8, I = usize>: Index<I, Output = T>
where
    T: Gene,
{
    /// make one mutation
    fn mutate<R: Rng>(&mut self, rng: &mut R);
    fn len(&self) -> I;
}

// impl DnaData

// TODO: Warning!!! Vec can have ->infinity len
impl<T> DnaData<T> for Vec<T>
where
    T: Gene,
{
    fn mutate<R: Rng>(&mut self, rng: &mut R) {
        let len = self.len();
        let variants = len + 2; // + inc/dec chance
        let v_index = rng.gen_range(0, variants);

        // mutate gene
        if v_index < len {
            let index = v_index;
            self[index].mutate(rng);
        } else {
            let other_mut = v_index - len;
            match other_mut {
                0 => self.push(T::create(rng)),
                1 => {
                    self.pop();
                }
                _ => unreachable!(),
            }
        }
    }

    fn len(&self) -> usize {
        self.len()
    }
}

impl<T> DnaData<T> for [T]
where
    T: Gene,
{
    fn mutate<R: Rng>(&mut self, rng: &mut R) {
        let index = rng.gen_range(0, self.len());
        self[index].mutate(rng);
    }

    fn len(&self) -> usize {
        self.len()
    }
}

pub trait DnaIndex: AddAssign + One {
    fn loop_on_size(&mut self, limit: &Self);
    fn inc_on_size(&mut self, limit: &Self) {
        self.add_assign(Self::one());
        self.loop_on_size(limit);
    }
}

macro_rules! impl_dna_index_for_num {
    ($ty:ty) => {
        impl DnaIndex for $ty {
            #[inline(always)]
            fn loop_on_size(&mut self, limit: &Self) {
                *self = *self % *limit
            }
        }
    };
}

impl_dna_index_for_num!(usize);
impl_dna_index_for_num!(isize);
impl_dna_index_for_num!(u32);
impl_dna_index_for_num!(i32);
impl_dna_index_for_num!(u16);
impl_dna_index_for_num!(i16);
impl_dna_index_for_num!(u8);
impl_dna_index_for_num!(i8);

// --------------
// Dna
// --------------

pub struct Dna<D, I>
where
    D: DnaData<D, I>,
    I: DnaIndex + Zero,
{
    data: D,
    ptr: I,
}

impl<D, I> Dna<D, I>
where
    D: DnaData<D, I>,
    I: DnaIndex + Zero,
{
    pub fn new(data: D) -> Self {
        Self {
            data,
            ptr: I::zero(),
        }
    }

    pub fn ptr_ref(&self) -> &I {
        &self.ptr
    }

    pub fn ptr(&self) -> I
    where
        I: Copy,
    {
        self.ptr
    }

    pub fn set_ptr(&mut self, mut ptr: I) {
        ptr.loop_on_size(self.data.len());
        self.ptr = ptr
    }

    pub fn read(&mut self) -> &D {}
}

pub type Dna256 = Dna<[u8; 256], u8>;
