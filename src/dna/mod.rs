use crate::pos::looped;
use rand::prelude::*;
use std::borrow::Borrow;
use std::ops::{Index, IndexMut};
use std::rc::Rc;

// ----
// Gene
// ----

pub trait Gene {
    fn mutate<R: Rng>(&mut self, rng: &mut R);
    fn create<R: Rng>(rng: &mut R) -> Self;
}

impl<T> Gene for T
where
    rand::distributions::Standard: rand::distributions::Distribution<T>,
{
    fn mutate<R: Rng>(&mut self, rng: &mut R) {
        *self = rng.gen()
    }

    fn create<R: Rng>(rng: &mut R) -> Self {
        rng.gen()
    }
}

// --------------
// Dna
// --------------

#[derive(Clone)]
pub struct Dna256 {
    data: Rc<[u8; 256]>,
    ptr: u8,
}

impl Dna256 {
    pub fn new() -> Self {
        Self {
            data: Rc::new([0u8; 256]),
            ptr: 0,
        }
    }

    pub fn with_rc(data: Rc<[u8; 256]>, ptr: u8) -> Self {
        let ptr = ((ptr as usize) % data.len()) as u8;
        Self { data, ptr }
    }

    pub fn from_rng<R: Rng + ?Sized>(rng: &mut R) -> Self {
        let mut data = Box::new([0u8; 256]);
        rng.fill_bytes(data.as_mut());
        Self {
            data: Rc::from(data),
            ptr: 0,
        }
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.data.len()
    }

    #[inline(always)]
    pub fn ptr(&self) -> usize {
        self.ptr as _
    }

    #[inline(always)]
    pub fn set_ptr(&mut self, ptr: usize) {
        self.ptr = ptr as u8;
    }

    #[inline(always)]
    pub fn skip(&mut self, offset: usize) {
        self.set_ptr(self.ptr() + offset)
    }

    pub fn skipi(&mut self, offset: isize) {
        let ptr = looped(self.ptr as isize + offset, 0..256);
        self.set_ptr(ptr as _);
    }

    #[inline(always)]
    pub fn data(&self) -> &[u8; 256] {
        self.data.borrow()
    }

    #[inline(always)]
    pub fn data_mut(&mut self) -> &mut [u8; 256] {
        Rc::make_mut(&mut self.data)
    }

    #[inline(always)]
    pub fn read_u8(&mut self) -> u8 {
        if self.data.len() != 0 {
            let r = self.data[self.ptr()];
            self.skip(1);
            r
        } else {
            0
        }
    }

    #[inline(always)]
    pub fn read_i8(&mut self) -> i8 {
        self.read_u8() as i8
    }

    #[inline(always)]
    pub fn read_u16(&mut self) -> u16 {
        (self.read_u8() as u16) << 8 | (self.read_u8() as u16) << 0
    }

    pub fn mutate<R: Rng>(&mut self, rng: &mut R) {
        let index = rng.gen_range(0, self.data.len());
        let data = Rc::<[u8; 256]>::make_mut(&mut self.data);
        data[index] = rng.gen();
    }

    pub fn mutate_with_chance<R: Rng>(&mut self, rng: &mut R, chance: f64) {
        if rng.gen_bool(chance) {
            self.mutate(rng);
        }
    }
}

impl Dna256 {
    #[inline(always)]
    fn loop_idx(&self, i: usize) -> usize {
        i % 256
    }
}

impl From<&[u8]> for Dna256 {
    fn from(src: &[u8]) -> Self {
        if src.len() > 256 {
            panic!("Slice len gt 256.")
        }
        let mut data = Box::new([0u8; 256]);
        for i in 0..src.len() {
            data[i] = src[i];
        }
        Self {
            data: Rc::from(data),
            ptr: 0,
        }
    }
}

impl Index<usize> for Dna256 {
    type Output = u8;

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[self.loop_idx(index)]
    }
}

impl IndexMut<usize> for Dna256 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        let i = self.loop_idx(index);
        &mut self.data_mut()[i]
    }
}
