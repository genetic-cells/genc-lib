#[macro_export]
macro_rules! pos {
    ($x: expr, $y: expr) => {
        crate::pos::Pos { x: $x, y: $y }
    };
}
