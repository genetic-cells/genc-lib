pub mod codes;
pub mod dna;
pub mod entity;
pub mod macros;
pub mod pos;
pub mod processor;
pub mod simulation;
pub mod updater;
pub mod utils;
pub mod world;

/// Треит перевода значения в нормированное представление его доступных значений 0.0..1.0
pub trait Norm {
    fn norm(&self) -> f64;
    fn norm_f32(&self) -> f32 {
        self.norm() as f32
    }
}

impl Norm for u8 {
    fn norm(&self) -> f64 {
        *self as f64 / 255.0
    }
}
