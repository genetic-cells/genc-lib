use crate::entity::Entity;
use crate::pos::Pos;
use ndarray::Array2;

use rand::Rng;

use crate::utils::iter::product;
use std::ops::{Index, IndexMut};

pub struct World {
    size: [usize; 2],
    entities: OptionLayer<Entity>,
    terrain: Layer<WCell>,
}

impl World {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            size: [width, height],
            terrain: Layer::new((width, height)),
            entities: OptionLayer::new((width, height)),
        }
    }

    pub fn size(&self) -> [usize; 2] {
        self.size
    }

    pub fn width(&self) -> usize {
        self.size[0]
    }
    pub fn height(&self) -> usize {
        self.size[1]
    }

    pub fn entities(&self) -> &OptionLayer<Entity> {
        &self.entities
    }
    pub fn entities_mut(&mut self) -> &mut OptionLayer<Entity> {
        &mut self.entities
    }

    pub fn terrain(&self) -> &Layer<WCell> {
        &self.terrain
    }
    pub fn terrain_mut(&mut self) -> &mut Layer<WCell> {
        &mut self.terrain
    }

    pub fn cell(&self, pos: Pos) -> &WCell {
        &self.terrain()[pos]
    }
    pub fn cell_mut(&mut self, pos: Pos) -> &mut WCell {
        &mut self.terrain_mut()[pos]
    }

    pub fn population(&self) -> usize {
        self.entities.some_count()
    }

    pub fn gen_population<R: Rng + ?Sized>(&mut self, rng: &mut R, count: usize) {
        for _ in 0..count {
            let x = rng.gen_range(0, self.width()) as isize;
            let y = rng.gen_range(0, self.height()) as isize;

            if self.entities().is_none([x, y].into()) {
                let e = Entity::from_rng(rng);
                self.entities_mut().replace([x, y].into(), e);
            }
        }
    }

    pub fn gen_start_population<R: Rng + ?Sized>(&mut self, rng: &mut R) {
        let count = self.height() * self.width() / 8;
        self.gen_population(rng, count);
    }
}

#[derive(Default, Clone)]
pub struct WCell {
    pub light: u8,
    pub food: u8,
    pub data: u8,
}

impl WCell {}

pub struct Layer<T> {
    array: Array2<T>,
}

impl<T> Layer<T> {
    //    pub fn from_fn<F>(size: (usize, usize), f: F) -> Self where F: FnMut(Pos) -> T {
    //        Self {
    //            array: Array2::from_shape_fn(size, |pos| {
    //                f(pos!(pos.0 as isize, pos.1 as isize))
    //            })
    //        }
    //    }

    pub fn width(&self) -> usize {
        self.array.raw_dim()[0]
    }

    pub fn height(&self) -> usize {
        self.array.raw_dim()[1]
    }

    pub fn size(&self) -> (isize, isize) {
        let dim = self.array.raw_dim();
        (dim[0] as isize, dim[1] as isize)
    }

    pub fn map_mut<F>(&mut self, mut mapper: F)
    where
        F: FnMut(&mut T, Pos),
    {
        for x in 0..self.size().0 {
            for y in 0..self.size().1 {
                let pos = Pos::new(x, y);
                mapper(&mut self.array[(pos.x as usize, pos.y as usize)], pos);
            }
        }
    }
}

impl<T> Layer<T>
where
    T: Clone,
{
    pub fn from_elem(size: (usize, usize), elem: T) -> Self {
        Layer {
            array: Array2::from_elem(size, elem),
        }
    }
}

impl<T> Layer<T>
where
    T: Default + Clone,
{
    pub fn new(size: (usize, usize)) -> Self {
        Layer {
            array: Array2::from_elem(size, T::default()),
        }
    }
}

impl<T> Index<Pos> for Layer<T> {
    type Output = T;

    fn index(&self, pos: Pos) -> &Self::Output {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);
        &self.array[(pos.x as usize, pos.y as usize)]
    }
}

impl<T> IndexMut<Pos> for Layer<T> {
    fn index_mut(&mut self, pos: Pos) -> &mut Self::Output {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);
        &mut self.array[(pos.x as usize, pos.y as usize)]
    }
}

pub struct OptionLayer<T> {
    layer: Layer<Option<T>>,
    some: usize,
}

impl<T> OptionLayer<T>
where
    T: Clone,
{
    pub fn new(size: (usize, usize)) -> Self {
        Self {
            layer: Layer::from_elem(size, Option::None),
            some: 0,
        }
    }
}

impl<T> OptionLayer<T> {
    pub fn size(&self) -> (isize, isize) {
        self.layer.size()
    }

    pub fn some_count(&self) -> usize {
        self.some
    }

    pub fn is_some(&self, pos: Pos) -> bool {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);
        self.layer[pos].is_some()
    }

    pub fn is_none(&self, pos: Pos) -> bool {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);
        self.layer[pos].is_none()
    }

    pub fn get(&self, pos: Pos) -> Option<&T> {
        self.layer[pos].as_ref()
    }

    pub fn get_mut(&mut self, pos: Pos) -> Option<&mut T> {
        self.layer[pos].as_mut()
    }

    pub fn replace(&mut self, pos: Pos, value: T) -> Option<T> {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);

        let last = self.layer[pos].replace(value);
        if let None = last {
            self.some += 1;
        }
        last
    }

    pub fn add(&mut self, pos: Pos, value: T) -> Result<(), T> {
        let cell = &mut self.layer[pos];
        if cell.is_none() {
            *cell = Some(value);
            self.some += 1;
            Ok(())
        } else {
            Err(value)
        }
    }

    pub fn take(&mut self, pos: Pos) -> Option<T> {
        let (w, h) = self.size();
        let pos = pos.loop_on([0..w, 0..h]);

        let last = self.layer[pos].take();
        if let Some(_) = last {
            self.some -= 1;
        }
        last
    }

    pub fn get_somes(&self, buf: &mut Vec<Pos>) {
        let w = self.layer.width();
        let h = self.layer.height();

        for (x, y) in product(0..w, 0..h) {
            let p = Pos::new(x as _, y as _);
            if self.get(p).is_some() {
                buf.push(p)
            }
        }
    }
}

// pub struct SomeIterMut<'a, T> {
//     layer: &'a mut OptionLayer<T>,
//     current: Pos,
//     is_end: bool,
// }
//
// impl<'a, T> Iterator for &'a mut SomeIterMut<'_, T> {
//     type Item = (Pos, &'a mut T);
//
//     fn next(&mut self) -> Option<Self::Item> {
//         let this = *self;
//         let (w, h) = this.layer.size() as (isize, isize);
//         let inc = |this: &'a mut SomeIterMut<'_, T>| {
//             if this.is_end {
//                 return;
//             }
//             let current = &mut this.current;
//             current.x += 1;
//             if current.x >= w {
//                 current.x = 0;
//                 current.y += 1;
//             }
//             if current.y >= h {
//                 this.is_end = true;
//             }
//         };
//         while !this.is_end {
//             if this.layer.get(this.current).is_some() {
//                 return this.layer.get_mut(this.current).map(|e| (this.current, e));
//             }
//             inc(this)
//         }
//         return None;
//     }
// }

// pub struct WCellRef<'a> {
//     w: &'a World,
//     pos: Pos,
// }
//
// impl<'a> WCellRef<'a> {
//     pub fn entity() -> &Entity {
//
//     }
// }
