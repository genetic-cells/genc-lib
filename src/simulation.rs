use crate::updater::Updater;
use crate::world::World;
use rand::prelude::SeedableRng;
use rand::prelude::SmallRng;
use rand::RngCore;

pub struct Simulation {
    world: World,
    updater: Updater,
    seed: u64,
    rng: SmallRng,
    tick: u64,
}

impl Simulation {
    pub fn new(world: World, updater: Updater) -> Self {
        Self::with_seed(world, updater, SmallRng::from_entropy().next_u64())
    }

    pub fn with_seed(world: World, updater: Updater, seed: u64) -> Self {
        let rng = SmallRng::seed_from_u64(seed);
        Self {
            world,
            updater,
            seed,
            rng,
            tick: 0,
        }
    }

    pub fn world(&self) -> &World {
        &self.world
    }

    pub fn world_mut(&mut self) -> &mut World {
        &mut self.world
    }

    pub fn updater(&self) -> &Updater {
        &self.updater
    }

    pub fn updater_mut(&mut self) -> &mut Updater {
        &mut self.updater
    }

    pub fn seed(&self) -> u64 {
        self.seed
    }

    pub fn reset(&mut self) {
        unimplemented!()
    }

    pub fn tick(&mut self) {
        self.tick += 1;
        self.updater.update(&mut self.world, &mut self.rng)
    }

    pub fn ticks(&self) -> u64 {
        self.tick
    }
}
