pub mod iter {
    /// Итератор перестановок каждого элемента левого итератора с каждым элементом правого итератора
    pub fn product<T, U>(
        lhs: impl IntoIterator<Item = T> + Clone,
        rhs: impl IntoIterator<Item = U> + Clone,
    ) -> impl Iterator<Item = (T, U)>
    where
        T: Clone,
        U: Clone,
    {
        lhs.into_iter()
            .flat_map(move |i| rhs.clone().into_iter().map(move |j| (i.clone(), j)))
    }
}
